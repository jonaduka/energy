package com.data;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.AssetManager;

import com.example.model.rooms;
import com.example.model.enums.RoomType;
import com.example.model.interfaces.IRooms;


public class parseRoomsCSV {

	public List<IRooms> run(String csvFile, Context context) {
		
		List<IRooms> roomsList = new ArrayList<IRooms>();
		 
		//String csvFile = "assets/floorThree.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		

			try {
				InputStream  is= getClass().getResourceAsStream("/assets/" + csvFile);
				//FileInputStream fis = context.openFileInput(is.toString()); 
				//FileInputStream fis = context.openFileInput(csvFile);  
                br = new BufferedReader(new InputStreamReader(is));

				//br = new BufferedReader(new FileReader(csvFile));
				Boolean isNotFirstLine = false;
				while ((line = br.readLine()) != null) {
					
		 			if(isNotFirstLine){
				        // use comma as separator
						String[] Room = line.split(cvsSplitBy);
						
						IRooms newRoom = new rooms();
						//System.out.println(Room);
						
						newRoom.setId(Integer.parseInt(Room[0]));
						newRoom.setFloor(Integer.parseInt(Room[1]));
						newRoom.setRoom(Integer.parseInt(Room[2]));
						newRoom.setRoomType(getType(Room[3]));
												
						roomsList.add(newRoom);
						//System.out.println("Quadrant: " + newRoom.getQuadrant());
						//System.out.println("Floor: " + newRoom.getFloor() + " Room: " + newRoom.getRoom());
						
					}

		 			else {
		 			isNotFirstLine = true;
		 			}
				}
		 
			} catch (FileNotFoundException e) {
				System.out.println("HERE IS THE FILE NOT FOUND EXCEPTION: " + e);
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		 
		if(roomsList.isEmpty()){
			return null;
			
		}
		else {
			//System.out.println(roomsList.size());
			return roomsList;
		}
	}
	
	

	private RoomType getType(String string) {
		//Office, Storage, Kitchen, Meeting, Printing, Subject, Pantry;
		if (string.equals("Office")){
			return RoomType.Office;
		}
		if (string.equals("Storage")){
			return RoomType.Storage;
		}
		if (string.equals("Kitchen")){
			return RoomType.Kitchen;
		}
		if (string.equals("Meeting")){
			return RoomType.Meeting;
		}
		if (string.equals("Printing")){
			return RoomType.Printing;
		}
		if (string.equals("Subject")){
			return RoomType.Subject;
		}
		if (string.equals("Pantry")){
			return RoomType.Pantry;
		}
		if (string.equals("Open")){
			return RoomType.Open;
		}
		else {
			System.out.println("Bad Room Type: " + string);
			return null;
			
		}

	}
	
}

package com.test;

import java.util.Calendar;

import org.junit.Ignore;
import org.junit.Test;

import com.example.energy.Solver.CalculateAlerts;
import com.example.model.enums.Job;

import junit.framework.TestCase;

public class testNotifications extends TestCase {

	//THIS IS NOT GONNA WORK COS IT NEEDS CONTEXT but it should be ok for now
	@Ignore
	public void testGetTimeOfDay() {
		
		CalculateAlerts ca = new CalculateAlerts();
		int c = ca.getTimeOfDay();
		assertTrue(c==0 || c==1 || c==2 || c==3 || c==4);
		System.out.println(c);
		
	}

	@Test
	public void testGetTypeOfJob(){
		CalculateAlerts ca = new CalculateAlerts();
		Job cZero = ca.getTypeOfJob(0);
		assertTrue(cZero.equals(Job.Heating) || cZero.equals(Job.Lights) || 
				cZero.equals(Job.Projectors) || cZero.equals(Job.Appliance));
		Job cTwo = ca.getTypeOfJob(2);
		assertTrue(cTwo.equals(Job.Appliance) || cTwo.equals(Job.Chargers));
	}
}

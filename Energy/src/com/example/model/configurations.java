package com.example.model;

import com.example.model.interfaces.IConfigurations;

public class configurations implements IConfigurations {
	
	Boolean lightsOn;
	Boolean heatingOn;
	Boolean projectorsOn;
	Boolean chargersOn;
	Boolean computersOn;
	Boolean windowsOn;
	//for sounds
	Boolean toneOn;
	
	public void initialize(){
		this.lightsOn = true;
		this.heatingOn = true;
		this.projectorsOn = true;
		this.chargersOn = true;
		this.computersOn = true;
		this.windowsOn = true;
		this.toneOn = true;
	}
	
	public Boolean getLightsOn() {
		return lightsOn;
	}
	public void setLightsOn(Boolean lightsOn) {
		this.lightsOn = lightsOn;
	}
	public Boolean getHeatingOn() {
		return heatingOn;
	}
	public void setHeatingOn(Boolean heatingOn) {
		this.heatingOn = heatingOn;
	}
	public Boolean getProjectorsOn() {
		return projectorsOn;
	}
	public void setProjectorsOn(Boolean projectorsOn) {
		this.projectorsOn = projectorsOn;
	}
	public Boolean getChargersOn() {
		return chargersOn;
	}
	public void setChargersOn(Boolean chargersOn) {
		this.chargersOn = chargersOn;
	}
	public Boolean getComputersOn() {
		return computersOn;
	}
	public void setComputersOn(Boolean computersOn) {
		this.computersOn = computersOn;
	}
	public Boolean getWindowsOn() {
		return windowsOn;
	}
	public void setWindowsOn(Boolean windowsOn) {
		this.windowsOn = windowsOn;
	}
	public Boolean getToneOn() {
		return toneOn;
	}
	public void setToneOn(Boolean toneOn) {
		this.toneOn = toneOn;
	}
	
	

}

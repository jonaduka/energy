package com.example.model.databaseHelpers;

import java.util.ArrayList;
import java.util.List;

import com.example.model.configurations;
import com.example.model.workingHours;
import com.example.model.enums.Day;
import com.example.model.interfaces.IConfigurations;
import com.example.model.interfaces.IWorkingHours;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class WorkingHoursHelper extends SQLiteOpenHelper{

	public static final String TABLE_WORKINGHOURS = "workinghours";
	 public static final String COLUMN_DAY = "day";
	 public static final String COLUMN_ISWORKING = "isworking";
	 public static final String COLUMN_START = "start";
	 public static final String COLUMN_END = "end";


	  private static final String DATABASE_NAME = "WorkingHoursHelper";
	  private static final int DATABASE_VERSION = 1;

	  public WorkingHoursHelper(Context context) {
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	  }

	  @Override
	  public void onCreate(SQLiteDatabase database) {
	    String DATABASE_CREATE = "create table "
	      + TABLE_WORKINGHOURS + "("
	      + COLUMN_DAY + " STRING," + COLUMN_ISWORKING + " INTEGER," + COLUMN_START + " INTEGER," + COLUMN_END + " INTEGER" + ");";
	    database.execSQL(DATABASE_CREATE);
	  }

	  @Override
	  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    //Log.w(ConfigurationsHelper.class.getName(),
	    //    "Upgrading database from version " + oldVersion + " to "
	    //        + newVersion + ", which will destroy all old data");
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_WORKINGHOURS);
	    onCreate(db);
	  }
	  
	//add workinghours
			public void addWorkingHours(IWorkingHours hours) {
				SQLiteDatabase db = this.getWritableDatabase();

				ContentValues values = new ContentValues();
				IWorkingHours w = new workingHours();
				values.put(COLUMN_DAY, (w.toIntFromDay(hours.getDay())));
				values.put(COLUMN_ISWORKING, hours.getIsOn());
				values.put(COLUMN_START, hours.getStart());
				values.put(COLUMN_END, hours.getEnd());
				

				// Inserting Row
				db.insert(TABLE_WORKINGHOURS, null, values);
				db.close(); // Closing database connection
			}

			
			// Getting All Candidates
			public List<IWorkingHours> getAllWorkingHours() {
				List<IWorkingHours> hoursList = new ArrayList<IWorkingHours>();
				// Select All Query
				String selectQuery = "SELECT  * FROM " + TABLE_WORKINGHOURS;

				SQLiteDatabase db = this.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);

				// looping through all rows and adding to list
				if (cursor.moveToFirst()) {
					do {
						IWorkingHours hours = new workingHours();
						hours.setDay(hours.toDayFromInt(cursor.getInt(0)));
					    hours.setIsOn(cursor.getInt(1) == 1);
					    hours.setStart((Integer) cursor.getInt(2));
					    hours.setEnd((Integer) cursor.getInt(3));

						// Adding contact to list
						hoursList.add(hours);
						
					} while (cursor.moveToNext());
				}
				db.close();
				// return contact list
				return hoursList;
			}

			public void deleteAll() {
				SQLiteDatabase db = this.getReadableDatabase();
				db.delete(TABLE_WORKINGHOURS, null, null);
			}
			
			//updateValue
			public void updateValue(Day day, Boolean isOn, int start, int end){
			
				SQLiteDatabase db = this.getWritableDatabase();
				
				IWorkingHours w = new workingHours();
				
				ContentValues values = new ContentValues();
				  values.put(COLUMN_ISWORKING, isOn);
				  values.put(COLUMN_START, start);
				  values.put(COLUMN_END, end);
				  db.update(TABLE_WORKINGHOURS, values, COLUMN_DAY + " = " + w.toIntFromDay(day),  null);
				
				//candidate.setRating(cursor.getInt(6));
			      db.close();
			}

}



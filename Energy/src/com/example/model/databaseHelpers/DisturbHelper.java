package com.example.model.databaseHelpers;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.model.doNotDisturb;
import com.example.model.workingHours;
import com.example.model.enums.Day;
import com.example.model.interfaces.IDoNotDisturb;
import com.example.model.interfaces.IWorkingHours;

public class DisturbHelper extends SQLiteOpenHelper {
	
	public static final String TABLE_DISTURB = "disturbHours";
	 public static final String COLUMN_START = "start";
	 public static final String COLUMN_END = "end";


	  private static final String DATABASE_NAME = "DisturbHelper";
	  private static final int DATABASE_VERSION = 1;

	  public DisturbHelper(Context context) {
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	  }

	  @Override
	  public void onCreate(SQLiteDatabase database) {
	    String DATABASE_CREATE = "create table "
	      + TABLE_DISTURB + "("
	      + COLUMN_START + " INTEGER," + COLUMN_END + " INTEGER" + ");";
	    database.execSQL(DATABASE_CREATE);
	  }

	  @Override
	  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    //Log.w(ConfigurationsHelper.class.getName(),
	    //    "Upgrading database from version " + oldVersion + " to "
	    //        + newVersion + ", which will destroy all old data");
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_DISTURB);
	    onCreate(db);
	  }
	  
	//add workinghours
			public void addWorkingHours(IDoNotDisturb hours) {
				SQLiteDatabase db = this.getWritableDatabase();

				ContentValues values = new ContentValues();
				IDoNotDisturb w = new doNotDisturb();
				values.put(COLUMN_START, hours.getStart());
				values.put(COLUMN_END, hours.getEnd());
				

				// Inserting Row
				db.insert(TABLE_DISTURB, null, values);
				db.close(); // Closing database connection
			}

			
			// Getting All Candidates
			public List<IDoNotDisturb> getAllDisturbs() {
				List<IDoNotDisturb> hoursList = new ArrayList<IDoNotDisturb>();
				// Select All Query
				String selectQuery = "SELECT  * FROM " + TABLE_DISTURB;

				SQLiteDatabase db = this.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);

				// looping through all rows and adding to list
				if (cursor.moveToFirst()) {
					do {
						IDoNotDisturb hours = new doNotDisturb();
					    hours.setStart((Integer) cursor.getInt(0));
					    hours.setEnd((Integer) cursor.getInt(1));

						// Adding contact to list
						hoursList.add(hours);
						
					} while (cursor.moveToNext());
				}
				db.close();
				// return contact list
				return hoursList;
			}

			public void deleteAll() {
				SQLiteDatabase db = this.getReadableDatabase();
				db.delete(TABLE_DISTURB, null, null);
			}
			
			//updateValue
			//there might be something wrong here... human error can screw this up
			public void deleteValue(int start){
				SQLiteDatabase db = this.getReadableDatabase();
				db.delete(TABLE_DISTURB, COLUMN_START + "=" + start , null);
			}

}

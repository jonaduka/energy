package com.example.model.databaseHelpers;

import java.util.ArrayList;
import java.util.List;

import com.example.model.achievement;
import com.example.model.configurations;
import com.example.model.interfaces.IAchievement;
import com.example.model.interfaces.IConfigurations;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class AchievementsHelper extends SQLiteOpenHelper{
	
	public static final String TABLE_ACHIEVEMENTS = "achieve";
	 public static final String COLUMN_KETTLES = "kettles";
	 public static final String COLUMN_LIGHTS = "lights";
	 public static final String COLUMN_TVS = "tvs";


	  private static final String DATABASE_NAME = "AchieveHelper";
	  private static final int DATABASE_VERSION = 1;

	  public AchievementsHelper(Context context) {
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	  }

	  @Override
	  public void onCreate(SQLiteDatabase database) {
	    String DATABASE_CREATE = "create table "
	      + TABLE_ACHIEVEMENTS + "("
	      + COLUMN_KETTLES + " INTEGER," + COLUMN_LIGHTS + " INTEGER," + COLUMN_TVS + " INTEGER" + ");";
	    database.execSQL(DATABASE_CREATE);
	  }

	  @Override
	  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACHIEVEMENTS);
	    onCreate(db);
	  }


	  /*
		public boolean isEmpty() {
			String countQuery = "SELECT  * FROM " + TABLE_CONFIGURATIONS;
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery(countQuery, null);
			//cursor.close();

			return (cursor.getCount()>0);
		}*/

		
		//add configs
		public void addAchievement(IAchievement a) {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(COLUMN_KETTLES, a.getKettles());
			values.put(COLUMN_LIGHTS, a.getLightbulbs());
			values.put(COLUMN_TVS, a.getTelevision());


			// Inserting Row
			db.insert(TABLE_ACHIEVEMENTS, null, values);
			db.close(); // Closing database connection
		}
		
		// Getting All Candidates
		public List<IAchievement> getAllAchievements() {
			List<IAchievement> achieveList = new ArrayList<IAchievement>();
			// Select All Query
			String selectQuery = "SELECT  * FROM " + TABLE_ACHIEVEMENTS;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					IAchievement a = new achievement();
					
					a.setKettles(cursor.getInt(0));
				    a.setLightbulbs(cursor.getInt(1));
				    a.setTelevision(cursor.getInt(2));

					// Adding contact to list
					achieveList.add(a);
					
				} while (cursor.moveToNext());
			}
			db.close();
			// return contact list
			return achieveList;
		}
	
		
		//updateValue
		public void updateValue(String config, Boolean value){
		
			SQLiteDatabase db = this.getWritableDatabase();
			
			
			ContentValues values = new ContentValues();
			  values.put(config, value);
			  db.update(TABLE_ACHIEVEMENTS, values, null, null);
			
			//candidate.setRating(cursor.getInt(6));
		      db.close();
		}
		
		public void deleteAll() {
			SQLiteDatabase db = this.getReadableDatabase();
			db.delete(TABLE_ACHIEVEMENTS, null, null);
		}
}


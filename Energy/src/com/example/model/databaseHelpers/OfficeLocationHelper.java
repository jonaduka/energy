package com.example.model.databaseHelpers;

import java.util.ArrayList;
import java.util.List;

import com.example.model.configurations;
import com.example.model.roomAllocations;
import com.example.model.workingHours;
import com.example.model.interfaces.IConfigurations;
import com.example.model.interfaces.IRoomAllocations;
import com.example.model.interfaces.IWorkingHours;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class OfficeLocationHelper extends SQLiteOpenHelper {
	
	 public static final String TABLE_OFFICELOCATION = "officelocation";
	 public static final String COLUMN_FIRSTNAME = "firstname";
	 public static final String COLUMN_SECONDNAME = "secondname";
	 public static final String COLUMN_FLOOR = "floor";
	 public static final String COLUMN_OFFICENUMBER = "officenumber";


	  private static final String DATABASE_NAME = "OfficeLocation";
	  private static final int DATABASE_VERSION = 1;

	  public OfficeLocationHelper(Context context) {
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	  }

	  @Override
	  public void onCreate(SQLiteDatabase database) {
	    String DATABASE_CREATE = "create table "
	      + TABLE_OFFICELOCATION + "("
	      + COLUMN_FIRSTNAME + " STRING," + COLUMN_SECONDNAME + " STRING," + COLUMN_FLOOR + " INTEGER," + COLUMN_OFFICENUMBER + " INTEGER" + ");";
	    database.execSQL(DATABASE_CREATE);
	  }

	  @Override
	  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    //Log.w(ConfigurationsHelper.class.getName(),
	    //    "Upgrading database from version " + oldVersion + " to "
	    //        + newVersion + ", which will destroy all old data");
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_OFFICELOCATION);
	    onCreate(db);
	  }


	  /*
		public boolean isEmpty() {
			String countQuery = "SELECT  * FROM " + TABLE_CONFIGURATIONS;
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery(countQuery, null);
			//cursor.close();

			return (cursor.getCount()>0);
		}*/

		
		//add configs
		public void addOfficeLocation(IRoomAllocations room) {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(COLUMN_FIRSTNAME, room.getFirstName());
			values.put(COLUMN_SECONDNAME, room.getSecondName());
			values.put(COLUMN_FLOOR, Math.round(room.getRoom()/100));
			values.put(COLUMN_OFFICENUMBER, room.getRoom() - (Math.round((room.getRoom()/100)*100)));
			

			// Inserting Row
			db.insert(TABLE_OFFICELOCATION, null, values);
			db.close(); // Closing database connection
		}
		
		// Getting All Candidates
		public List<IRoomAllocations> getAllOfficeLocations() {
			List<IRoomAllocations> roomList = new ArrayList<IRoomAllocations>();
			// Select All Query
			String selectQuery = "SELECT  * FROM " + TABLE_OFFICELOCATION;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					IRoomAllocations room = new roomAllocations();
					
					room.setFirstName(cursor.getString(0));
				    room.setSecondName(cursor.getString(1));
				    room.setRoom(cursor.getInt(2)*100 + cursor.getInt(3));
					// Adding contact to list
					roomList.add(room);
					
				} while (cursor.moveToNext());
			}
			db.close();
			// return contact list
			return roomList;
		}
	
		
		//updateValue *THIS WILL NEED CHANGING JUST COPIED FROM CONFIGS FOR NOW...
		public void updateValue(IRoomAllocations ra){
		
			SQLiteDatabase db = this.getWritableDatabase();
			
			
			ContentValues values = new ContentValues();
			  values.put(COLUMN_FIRSTNAME, ra.getFirstName());
			  values.put(COLUMN_SECONDNAME, ra.getSecondName());
			  values.put(COLUMN_FLOOR, Math.round(ra.getRoom()/100));
			  values.put(COLUMN_OFFICENUMBER, ra.getRoom() -(Math.round(ra.getRoom()/100)));
			  db.update(TABLE_OFFICELOCATION, values, null,  null);
			
			//candidate.setRating(cursor.getInt(6));
		      db.close();
		}

		public void deleteAll() {
			SQLiteDatabase db = this.getReadableDatabase();
			db.delete(TABLE_OFFICELOCATION, null, null);
		}

}

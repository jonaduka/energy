package com.example.model.databaseHelpers;

import java.util.ArrayList;
import java.util.List;

import com.example.model.configurations;
import com.example.model.interfaces.IConfigurations;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ConfigurationsHelper extends SQLiteOpenHelper{
	
	 public static final String TABLE_CONFIGURATIONS = "configurations";
	 public static final String COLUMN_LIGHTS = "lights";
	 public static final String COLUMN_HEATING = "heating";
	 public static final String COLUMN_PROJECTORS = "projectors";
	 public static final String COLUMN_CHARGERS = "chargers";
	 public static final String COLUMN_COMPUTERS = "computers";
	 public static final String COLUMN_WINDOWS = "windows";
	 public static final String COLUMN_TONE = "tone";
	 public static final String COLUMN_KEY = "key";

	  private static final String DATABASE_NAME = "ConfigurationsHelper";
	  private static final int DATABASE_VERSION = 1;

	  public ConfigurationsHelper(Context context) {
	    super(context, DATABASE_NAME, null, DATABASE_VERSION);
	  }

	  @Override
	  public void onCreate(SQLiteDatabase database) {
	    String DATABASE_CREATE = "create table "
	      + TABLE_CONFIGURATIONS + "("
	      + COLUMN_LIGHTS + " INTEGER," + COLUMN_HEATING + " INTEGER," + COLUMN_PROJECTORS + " INTEGER," + COLUMN_CHARGERS + " INTEGER,"
	      + COLUMN_COMPUTERS + " INTEGER," + COLUMN_WINDOWS + " INTEGER," + COLUMN_TONE + " INTEGER" + ");";
	    database.execSQL(DATABASE_CREATE);
	  }

	  @Override
	  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    //Log.w(ConfigurationsHelper.class.getName(),
	    //    "Upgrading database from version " + oldVersion + " to "
	    //        + newVersion + ", which will destroy all old data");
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONFIGURATIONS);
	    onCreate(db);
	  }


	  /*
		public boolean isEmpty() {
			String countQuery = "SELECT  * FROM " + TABLE_CONFIGURATIONS;
			SQLiteDatabase db = this.getReadableDatabase();
			Cursor cursor = db.rawQuery(countQuery, null);
			//cursor.close();

			return (cursor.getCount()>0);
		}*/

		
		//add configs
		public void addConfiguration(IConfigurations config) {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(COLUMN_LIGHTS, config.getLightsOn());
			values.put(COLUMN_HEATING, config.getHeatingOn());
			values.put(COLUMN_PROJECTORS, config.getProjectorsOn());
			values.put(COLUMN_CHARGERS, config.getChargersOn());
			values.put(COLUMN_COMPUTERS, config.getComputersOn());
			values.put(COLUMN_WINDOWS, config.getWindowsOn());
			values.put(COLUMN_TONE, config.getToneOn());
			

			// Inserting Row
			db.insert(TABLE_CONFIGURATIONS, null, values);
			db.close(); // Closing database connection
		}
		
		// Getting All Candidates
		public List<IConfigurations> getAllConfigurations() {
			List<IConfigurations> configList = new ArrayList<IConfigurations>();
			// Select All Query
			String selectQuery = "SELECT  * FROM " + TABLE_CONFIGURATIONS;

			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			// looping through all rows and adding to list
			if (cursor.moveToFirst()) {
				do {
					IConfigurations configurations = new configurations();
					
					configurations.setLightsOn(cursor.getInt(0) == 1);
				    configurations.setHeatingOn(cursor.getInt(1) == 1);
				    configurations.setProjectorsOn(cursor.getInt(2) == 1);
				    configurations.setChargersOn(cursor.getInt(3) == 1);
				    configurations.setComputersOn(cursor.getInt(4) == 1);
				    configurations.setWindowsOn(cursor.getInt(5) == 1);
				    configurations.setToneOn(cursor.getInt(6) == 1);
					// Adding contact to list
					configList.add(configurations);
					
				} while (cursor.moveToNext());
			}
			db.close();
			// return contact list
			return configList;
		}
	
		
		//updateValue
		public void updateValue(String config, Boolean value){
		
			SQLiteDatabase db = this.getWritableDatabase();
			
			
			ContentValues values = new ContentValues();
			  values.put(config, value);
			  db.update(TABLE_CONFIGURATIONS, values, null, null);
			
			//candidate.setRating(cursor.getInt(6));
		      db.close();
		}
}

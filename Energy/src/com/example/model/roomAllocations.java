package com.example.model;

import com.example.model.interfaces.IRoomAllocations;

public class roomAllocations implements IRoomAllocations{
	
	String firstName;
	String secondName;
	Integer room;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	public Integer getRoom() {
		return room;
	}
	public void setRoom(Integer room) {
		this.room = room;
	}

}

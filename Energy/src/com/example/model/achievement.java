package com.example.model;

import com.example.model.interfaces.IAchievement;

public class achievement implements IAchievement{
	
	Integer Kettles;
	Integer Lightbulbs;
	Integer Television;
	
	
	public achievement(){
	}
	
	public achievement(Integer k, Integer l, Integer t){
		this.Kettles=k;
		this.Lightbulbs=l;
		this.Television=t;
	}
	
	public Integer getKettles() {
		return Kettles;
	}

	public void setKettles(Integer kettles) {
		Kettles = kettles;
	}

	public Integer getLightbulbs() {
		return Lightbulbs;
	}

	public void setLightbulbs(Integer lightbulbs) {
		Lightbulbs = lightbulbs;
	}

	public Integer getTelevision() {
		return Television;
	}

	public void setTelevision(Integer television) {
		Television = television;
	}
	
	

}

package com.example.model;

import com.example.model.interfaces.IDoNotDisturb;

public class doNotDisturb implements IDoNotDisturb {
	
	Integer start;
	Integer end;
	
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getEnd() {
		return end;
	}
	public void setEnd(Integer end) {
		this.end = end;
	}
	

}

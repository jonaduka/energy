package com.example.model;

import com.example.model.interfaces.IRooms;
import com.example.model.enums.*;

public class rooms implements IRooms {
	
	Integer id;
	Integer floor;
	Integer Room;
	RoomType roomType;
	
	public rooms(){
	}
	
	public rooms(Integer id, Integer floor, Integer Room, RoomType roomType){
		this.id = id;
		this.floor = floor;
		this.Room = Room;
		this.roomType = roomType;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getFloor() {
		return floor;
	}
	public void setFloor(Integer floor) {
		this.floor = floor;
	}
	public Integer getRoom() {
		return Room;
	}
	public void setRoom(Integer room) {
		Room = room;
	}
	public RoomType getRoomType() {
		return roomType;
	}
	public void setRoomType(RoomType roomType) {
		this.roomType = roomType;
	}

	@Override
	public int getQuadrant() {
		int number = this.Room;
	    if(number<12){
	    	return 1;
	    }
	    if(13<number && number<23){
	    	return 2;
	    }
	    if(24<number && number<31){
	    	return 3;
	    }
	   	if(32<number){
	   		return 4;
	   	}
	   	else return 0;
	   	
	}
	

}

package com.example.model.enums;

public enum RoomType {
	
	Office, Storage, Kitchen, Meeting, Printing, Subject, Pantry, Open;

}

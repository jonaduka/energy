package com.example.model.enums;

public enum Job {
	Lights, Heating, Projectors, Appliance, Chargers, Computer;
}

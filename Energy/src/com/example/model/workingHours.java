package com.example.model;

import com.example.model.enums.*;
import com.example.model.interfaces.IWorkingHours;

public class workingHours implements IWorkingHours {
	
	Day day;
	Boolean isOn;
	Integer start;
	Integer end;
	
	public workingHours(Day day, Boolean isOn, Integer start, Integer end){
		this.day = day;
		this.isOn = isOn;
		this.start = start;
		this.end = end;
	}
	
	public workingHours(){
		
	}
	
	public Day getDay() {
		return day;
	}
	public void setDay(Day day) {
		this.day = day;
	}
	public Boolean getIsOn() {
		return isOn;
	}
	public void setIsOn(Boolean isOn) {
		this.isOn = isOn;
	}
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getEnd() {
		return end;
	}
	public void setEnd(Integer end) {
		this.end = end;
	}


	public Day toDayFromInt(int i) {
		if (i==1){
			return Day.Monday;
		}
		else if (i==2){
			return Day.Tuesday;
		}
		else if (i==3){
			return Day.Wednesday;
		}
		else if (i==4){
			return Day.Thursday;
		}
			return Day.Friday;

	}
	
	public Integer toIntFromDay(Day d){
		if (d==Day.Monday){
			return 1;
		}
		else if(d==Day.Tuesday){
			return 2;
		}
		else if(d==Day.Wednesday){
			return 3;
		}
		else if(d==Day.Thursday){
			return 4;
		}
			return 5;

	}


}

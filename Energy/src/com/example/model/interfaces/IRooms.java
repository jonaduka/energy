package com.example.model.interfaces;

import com.example.model.enums.*;;

public interface IRooms {
	
	public Integer getId();
	public void setId(Integer id);
	public Integer getFloor();
	public void setFloor(Integer floor);
	public Integer getRoom();
	public void setRoom(Integer room);
	public RoomType getRoomType();
	public void setRoomType(RoomType roomType);
	public int getQuadrant();

}

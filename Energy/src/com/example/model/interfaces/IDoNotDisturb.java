package com.example.model.interfaces;

public interface IDoNotDisturb {
	public Integer getStart();
	public void setStart(Integer start);
	public Integer getEnd();
	public void setEnd(Integer end);
}

package com.example.model.interfaces;

public interface IConfigurations {
	
	
	public void initialize();
	public Boolean getLightsOn();
	public void setLightsOn(Boolean lightsOn);
	public Boolean getHeatingOn();
	public void setHeatingOn(Boolean heatingOn);
	public Boolean getProjectorsOn();
	public void setProjectorsOn(Boolean projectorsOn);
	public Boolean getChargersOn();
	public void setChargersOn(Boolean chargersOn);
	public Boolean getComputersOn();
	public void setComputersOn(Boolean computersOn);
	public Boolean getWindowsOn();
	public void setWindowsOn(Boolean windowsOn);
	public Boolean getToneOn();
	public void setToneOn(Boolean toneOn);

}

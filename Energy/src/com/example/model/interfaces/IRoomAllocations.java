package com.example.model.interfaces;

public interface IRoomAllocations {
	
	public String getFirstName();
	public void setFirstName(String firstName);
	public String getSecondName();
	public void setSecondName(String secondName);
	public Integer getRoom();
	public void setRoom(Integer room);
}

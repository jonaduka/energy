package com.example.model.interfaces;

import java.util.Date;

import com.example.model.workingHours;
import com.example.model.enums.*;;

public interface IWorkingHours {
	
	public Day getDay();
	public void setDay(Day day);
	public Boolean getIsOn();
	public void setIsOn(Boolean isOn);
	public Integer getStart();
	public void setStart(Integer start);
	public Integer getEnd();
	public void setEnd(Integer integer);
	public Integer toIntFromDay(Day day);
	public Day toDayFromInt(int i);
}

package com.example.model.interfaces;

public interface IAlert {

	public String getDescription();
	public void setDescription(String description);
	public String getType();
	public void setType(String type);
	
}

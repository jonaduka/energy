package com.example.model.interfaces;

public interface IAchievement {
	
	

	public Integer getKettles();

	public void setKettles(Integer kettles);

	public Integer getLightbulbs();

	public void setLightbulbs(Integer lightbulbs);

	public Integer getTelevision();

	public void setTelevision(Integer television);

}

package com.example.energy;

import com.google.analytics.tracking.android.EasyTracker;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class HelpActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
	}
	
	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	  
	
	public void FAQ(View view){
    	//coming soon dialog
		new AlertDialog.Builder(this)
	    .setTitle("Coming Soon!").setNeutralButton("ok", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	            dialog.dismiss();
	        }
	     })
	    .setIcon(R.drawable.ic_action_star)
	     .show();
		
    }
	public void about(View view){
    	Intent intent = new Intent(this, AboutActivity.class);
    	startActivity(intent); 	
    }
	public void contact(View view){
    	Intent intent = new Intent(this, ContactActivity.class);
    	startActivity(intent); 	
    }

	public void doneHelp(View view){
    	Intent intent = new Intent(this, HomeActivity.class);
    	startActivity(intent); 	
    }

}

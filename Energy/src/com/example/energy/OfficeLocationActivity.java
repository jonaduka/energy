package com.example.energy;

import java.util.List;

import com.example.model.configurations;
import com.example.model.roomAllocations;
import com.example.model.workingHours;
import com.example.model.databaseHelpers.OfficeLocationHelper;
import com.example.model.interfaces.IConfigurations;
import com.example.model.interfaces.IRoomAllocations;
import com.example.model.interfaces.IWorkingHours;
import com.google.analytics.tracking.android.EasyTracker;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class OfficeLocationActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_office_location);
	
	
	EditText firstName = (EditText) findViewById(R.id.firstName);
	EditText secondName = (EditText) findViewById(R.id.secondName);
	EditText floor = (EditText) findViewById(R.id.floor);
	EditText officeNumber = (EditText) findViewById(R.id.officeNumber);
	
	final OfficeLocationHelper ol = new OfficeLocationHelper(this);
	List<IRoomAllocations> a = ol.getAllOfficeLocations();
	
	if (ol.getAllOfficeLocations().isEmpty()){
		firstName.setText("");
		secondName.setText("");
		floor.setText("");
		officeNumber.setText("");
	}
	else {
		//set it to saved values
		for (IRoomAllocations r : ol.getAllOfficeLocations()){
			firstName.setText(r.getFirstName());
			secondName.setText(r.getSecondName());
			char[] no = Integer.toString(r.getRoom()).toCharArray();
			String floorString = Character.toString(no[0]);
			String roomString = Character.toString(no[1]) + Character.toString(no[2]);
			floor.setText(floorString);
			officeNumber.setText(roomString);
		}
	}
	
	}
	
	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	  
	
	
	public void saveOfficeLocation(View view){
		//first check that all fields have information in them
		EditText firstName = (EditText) findViewById(R.id.firstName);
		EditText secondName = (EditText) findViewById(R.id.secondName);
		final EditText floor = (EditText) findViewById(R.id.floor);
		final EditText officeNumber = (EditText) findViewById(R.id.officeNumber);
		
		final OfficeLocationHelper ol = new OfficeLocationHelper(this);
		
		//check to make sure all fields got info
		if(!firstName.getText().toString().isEmpty()){
			if(!secondName.getText().toString().isEmpty()){
				if(!floor.getText().toString().isEmpty()){
					if(!officeNumber.getText().toString().isEmpty()){
						//delete all current entries
						
						
						try {
							//check that floor or officeNumber is only one number
						    Integer.parseInt(floor.getText().toString());
						    Integer.parseInt(officeNumber.getText().toString());
						    
						    ol.deleteAll();
							IRoomAllocations ra = new roomAllocations();
							ra.setFirstName(firstName.getText().toString());
							ra.setSecondName(secondName.getText().toString());
							Integer room = (Integer.parseInt(floor.getText().toString()) * 100) + Integer.parseInt(officeNumber.getText().toString());
							ra.setRoom(room);
							//save new values
							ol.addOfficeLocation(ra);
							Intent intent = new Intent(this, HomeActivity.class);
					    	startActivity(intent); 	

							
						} catch (NumberFormatException e) {
							new AlertDialog.Builder(this)
						    .setTitle("Incorrect floor or office type entered.").setNeutralButton("ok", new DialogInterface.OnClickListener() {
						    	 @Override
						         public void onClick(DialogInterface dialog, int which) {
						             dialog.dismiss();
						         }
						     })
						    .setIcon(R.drawable.ic_action_star)
						     .show();
						}
						
						
						
					}
				}
			}
		}

	    	
		
	}

}

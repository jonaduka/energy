package com.example.energy;
// WELCOME SCREEN

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.format.DateUtils;
import android.view.View;
import com.example.energy.R;
import com.example.energy.Notifications.NotificationActivity;
import com.example.energy.Notifications.NotificationRecieverActivity;
import com.example.energy.Notifications.TimeAlarm;
import com.example.energy.Solver.CalculateAlerts;
import com.example.model.alert;
import com.example.model.workingHours;
import com.example.model.databaseHelpers.DisturbHelper;
import com.example.model.databaseHelpers.OfficeLocationHelper;
import com.example.model.databaseHelpers.WorkingHoursHelper;
import com.example.model.enums.Day;
import com.example.model.interfaces.IAlert;
import com.example.model.interfaces.IDoNotDisturb;
import com.example.model.interfaces.IWorkingHours;
import com.google.analytics.tracking.android.EasyTracker;

public class MainActivity extends Activity {


	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

		
	
	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	  
	
    public void home(View view){
    	Intent intent = new Intent(this, HomeActivity.class);

		startActivity(intent);
		
    	
    }

}

package com.example.energy;

import java.util.List;

import com.example.model.achievement;
import com.example.model.configurations;
import com.example.model.databaseHelpers.AchievementsHelper;
import com.example.model.databaseHelpers.ConfigurationsHelper;
import com.example.model.interfaces.IAchievement;
import com.example.model.interfaces.IConfigurations;
import com.google.analytics.tracking.android.EasyTracker;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class AchievementsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_achievements);
		
		 TextView kettles = (TextView) findViewById(R.id.kettles);
		 TextView lights = (TextView) findViewById(R.id.lightbulbs);
		 TextView television = (TextView) findViewById(R.id.television);
		 
		 int K = 0;
		 int L = 0;
		 int T = 0;
		 
		 final AchievementsHelper achieve = new AchievementsHelper(this);
		 List<IAchievement> a =  achieve.getAllAchievements();
			
			if (a.size() == 0){
				//initialize the database all with 0 values
				IAchievement newAchieve = new achievement(0,0,0);
				achieve.addAchievement(newAchieve);
			}
			else {
				for (IAchievement aa : a){
					 
					K = aa.getKettles();
					L = aa.getLightbulbs();
					T = aa.getTelevision();
				}
			}
			
			kettles.setText("Boiling " + K + " kettles of tea, or ");
			lights.setText("Having a light bulb on for " + L + " hours, or ");
			television.setText("Leaving the television on for " + T + " hours.");

	}
	
	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	
	  public void doneAchievements(View view){
	    	Intent intent = new Intent(this, HomeActivity.class);
	    	startActivity(intent); 	
	    }


}

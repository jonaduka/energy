package com.example.energy;

import java.util.ArrayList;
import java.util.List;

import com.example.model.configurations;
import com.example.model.interfaces.IConfigurations;
import com.example.model.databaseHelpers.ConfigurationsHelper;
import com.google.analytics.tracking.android.EasyTracker;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;

public class ConfigurationsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_configurations);
		
	CheckBox lightsCheck = (CheckBox) findViewById(R.id.lightsCheck);
	CheckBox heatingCheck = (CheckBox) findViewById(R.id.heatingCheck);
	CheckBox projectorsCheck = (CheckBox) findViewById(R.id.projectorsCheck);
	CheckBox chargersCheck = (CheckBox) findViewById(R.id.chargersCheck);
	CheckBox computersCheck = (CheckBox) findViewById(R.id.computersCheck);
	CheckBox windowsCheck = (CheckBox) findViewById(R.id.windowsCheck);
	//CheckBox toneCheck = (CheckBox) findViewById(R.id.toneCheck);
	
	final ConfigurationsHelper config = new ConfigurationsHelper(this);
	
	if (config.getAllConfigurations().isEmpty()){
		//initialize the database all with true values
		IConfigurations newConfig = new configurations();
		newConfig.initialize();
		//add to the database
		config.addConfiguration(newConfig);
	}
	

	//there should be only one entry so this should work just fine
	for (IConfigurations c : config.getAllConfigurations()) {
		lightsCheck.setChecked(c.getLightsOn());
		heatingCheck.setChecked(c.getHeatingOn());
		projectorsCheck.setChecked(c.getProjectorsOn());
		chargersCheck.setChecked(c.getChargersOn());
		computersCheck.setChecked(c.getComputersOn());
		windowsCheck.setChecked(c.getWindowsOn());
		//toneCheck.setChecked(c.getToneOn());
	}
	
		// then we need to say what happens when things get clicked
	

		 
		lightsCheck.setOnClickListener(new OnClickListener() {
	 
		  @Override
		  public void onClick(View v) {
	                //is  checked?
			if (((CheckBox) v).isChecked()) {
				config.updateValue("lights", true);
			}
			else {
				config.updateValue("lights", false);	
			}
		  }
		  
		});
		
		heatingCheck.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) {
		                //is  checked?
				if (((CheckBox) v).isChecked()) {
					config.updateValue("heating", true);
				}
				else {
					config.updateValue("heating", false);	
				}
			  }
			  
			});
		
		projectorsCheck.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) {
		                //is  checked?
				if (((CheckBox) v).isChecked()) {
					config.updateValue("projectors", true);
				}
				else {
					config.updateValue("projectors", false);	
				}
			  }
			  
			});
		
		chargersCheck.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) {
		                //is  checked?
				if (((CheckBox) v).isChecked()) {
					config.updateValue("chargers", true);
				}
				else {
					config.updateValue("chargers", false);	
				}
			  }
			  
			});
		
		computersCheck.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) {
		                //is  checked?
				if (((CheckBox) v).isChecked()) {
					config.updateValue("computers", true);
				}
				else {
					config.updateValue("computers", false);	
				}
			  }
			  
			});
		
		windowsCheck.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) {
		                //is  checked?
				if (((CheckBox) v).isChecked()) {
					config.updateValue("windows", true);
				}
				else {
					config.updateValue("windows", false);	
				}
			  }
			  
			});

		/*toneCheck.setOnClickListener(new OnClickListener() {
			 
			  @Override
			  public void onClick(View v) {
		                //is  checked?
				if (((CheckBox) v).isChecked()) {
					config.updateValue("tone", true);
				}
				else {
					config.updateValue("tone", false);	
				}
			  }
			  
			});*/
		
	}
	
	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	  
	  public void doneConfig(View view){
	    	Intent intent = new Intent(this, SettingsHomeActivity.class);
	    	startActivity(intent); 	
	    }
		
}


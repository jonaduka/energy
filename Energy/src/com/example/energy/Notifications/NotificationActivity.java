package com.example.energy.Notifications;

import java.util.Calendar;

import com.example.energy.OfficeLocationActivity;
import com.example.energy.R;
import com.example.energy.Solver.CalculateAlerts;
import com.example.model.alert;
import com.example.model.workingHours;
import com.example.model.databaseHelpers.DisturbHelper;
import com.example.model.databaseHelpers.OfficeLocationHelper;
import com.example.model.databaseHelpers.WorkingHoursHelper;
import com.example.model.enums.Day;
import com.example.model.interfaces.IAlert;
import com.example.model.interfaces.IDoNotDisturb;
import com.example.model.interfaces.IWorkingHours;
import com.google.analytics.tracking.android.EasyTracker;

import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;


public class NotificationActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification);
	}
	
	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	
	
	
	  public void createNotification(View view){
		  createNotification();
	  }
	  
	//THIS IS THE SMART ONE WE WILL REALLY END UP USING
	public void createNotification(){
		Intent intent = new Intent(this, NotificationRecieverActivity.class);
		intent.putExtra("Alarm", "1");
		final Intent office = new Intent(this, OfficeLocationActivity.class);
		OfficeLocationHelper olh = new OfficeLocationHelper(this);
		//olh.deleteAll();
		//first make sure that office number has been entered.
		if(olh.getAllOfficeLocations().isEmpty()){
			//now is not a good time to have an alert. 
	    	new AlertDialog.Builder(this)
		    .setTitle("Your office location needs to be entered before we can create a notification").setNeutralButton("ok", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            startActivity(office);
		        }
		     })
		    .setIcon(R.drawable.ic_action_star)
		     .show();
		}
		
		else{
	    
	    CalculateAlerts calculator = new CalculateAlerts();
	    IAlert alert = new alert();
	    // check if it is in DO NOT DISTURB time
	    Boolean canBeDisturbed = true;
    	DisturbHelper hours = new DisturbHelper(this);
    	Calendar c = Calendar.getInstance();
        int hour   = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);
	    int now = hour*100 + minute;
	    
	    //hours.deleteAll();
	    //check if now is a time that we can disturb them or not.
	    for (IDoNotDisturb d : hours.getAllDisturbs()){
        	if(d.getStart()<now && now<d.getEnd()){
        		canBeDisturbed = false;
        	}
        }
	    
	    //check if it is weekday not weekend
	    int today = c.get(Calendar.DAY_OF_WEEK);
	    if ((today == 1) || (today == 7)){
	    	canBeDisturbed = false;
	    }
	    
	    //check if we are within working hours
	    IWorkingHours wh = new workingHours();
	    Day day = wh.toDayFromInt(today-1);
	    WorkingHoursHelper whh = new WorkingHoursHelper(this);
	    for (IWorkingHours a : whh.getAllWorkingHours()){
	    	if(a.getDay().equals(day)){
	    		if(a.getStart()>now || now>a.getEnd()){
	    			canBeDisturbed = false;
	    		}
	    		else if(!a.getIsOn()){
	    			canBeDisturbed = false;
	    		}
	    	}
	    }
	    
        
	    if(canBeDisturbed){
	    	alert = calculator.createAlert(this);
	    	Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

	    	//add stuff from alert to intent so it can be accessed in next page
	    	intent.putExtra("Type", alert.getType());
	    	intent.putExtra("Description", alert.getDescription());
	    	
	    PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	    // Build notification
	    // Actions are just fake.. just add button to database of counter.easy enough
	    Notification n = new Notification.Builder(this)
	        .setContentTitle("ALERT: " + alert.getType())
	        .setContentText(alert.getDescription())
	        .setSmallIcon(R.drawable.ic_action_star)
	        .setSound(soundUri)
	        .setContentIntent(pIntent)
	        .build();
	    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		// Hide the notification after its selected
	    n.flags |= Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(0, n);
		}
	    
	    else{
	    	//now is not a good time to have an alert. 
	    	new AlertDialog.Builder(this)
		    .setTitle("User can not be disturbed at the moment").setNeutralButton("ok", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            //do nothing
		        }
		     })
		    .setIcon(R.drawable.ic_action_star)
		     .show();
	    }
		}
	}

}

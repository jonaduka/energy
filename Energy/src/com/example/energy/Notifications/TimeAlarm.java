package com.example.energy.Notifications;

import java.util.Calendar;
import java.util.Random;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;

import com.example.energy.OfficeLocationActivity;
import com.example.energy.R;
import com.example.energy.Solver.CalculateAlerts;
import com.example.model.alert;
import com.example.model.workingHours;
import com.example.model.databaseHelpers.DisturbHelper;
import com.example.model.databaseHelpers.OfficeLocationHelper;
import com.example.model.databaseHelpers.WorkingHoursHelper;
import com.example.model.enums.Day;
import com.example.model.interfaces.IAlert;
import com.example.model.interfaces.IDoNotDisturb;
import com.example.model.interfaces.IWorkingHours;

	public class TimeAlarm extends BroadcastReceiver {

		NotificationManager nm;
		@Override
		 public void onReceive(Context context, Intent intent) {
		    
		    if(Math.random() < 0.5){
			
			Intent intent1 = new Intent(context, NotificationRecieverActivity.class);
			intent1.putExtra("Alarm", "1");
			final Intent office = new Intent(context, OfficeLocationActivity.class);
			OfficeLocationHelper olh = new OfficeLocationHelper(context);
			//olh.deleteAll();
			//first make sure that office number has been entered.
			if(olh.getAllOfficeLocations().isEmpty()){
				//do nothing
			}
			else{
		    
		    CalculateAlerts calculator = new CalculateAlerts();
		    IAlert alert = new alert();
		    // check if it is in DO NOT DISTURB time
		    Boolean canBeDisturbed = true;
	    	DisturbHelper hours = new DisturbHelper(context);
	    	Calendar c = Calendar.getInstance();
	        int hour   = c.get(Calendar.HOUR_OF_DAY);
	        int minute = c.get(Calendar.MINUTE);
		    int now = hour*100 + minute;
		    
		    //hours.deleteAll();
		    //check if now is a time that we can disturb them or not.
		    for (IDoNotDisturb d : hours.getAllDisturbs()){
	        	if(d.getStart()<now && now<d.getEnd()){
	        		canBeDisturbed = false;
	        	}
	        }
		    
		    //check if it is weekday not weekend
		    int today = c.get(Calendar.DAY_OF_WEEK);
		    if ((today == 1) || (today == 7)){
		    	canBeDisturbed = false;
		    }
		    
		    //check if we are within working hours
		    IWorkingHours wh = new workingHours();
		    Day day = wh.toDayFromInt(today-1);
		    WorkingHoursHelper whh = new WorkingHoursHelper(context);
		    for (IWorkingHours a : whh.getAllWorkingHours()){
		    	if(a.getDay().equals(day)){
		    		if(a.getStart()>now || now>a.getEnd()){
		    			canBeDisturbed = false;
		    		}
		    		else if(!a.getIsOn()){
		    			canBeDisturbed = false;
		    		}
		    	}
		    }
		    
	        
		    if(canBeDisturbed){
		    	alert = calculator.createAlert(context);
		    	Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		    	//add stuff from alert to intent so it can be accessed in next page
		    	intent1.putExtra("Type", alert.getType());
		    	intent1.putExtra("Description", alert.getDescription());
		    	
		    PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
		    // Build notification
		    // Actions are just fake.. just add button to database of counter.easy enough
		    Notification n = new Notification.Builder(context)
		        .setContentTitle("ALERT: " + alert.getType())
		        .setContentText(alert.getDescription())
		        .setSmallIcon(R.drawable.ic_action_star)
		        .setSound(soundUri)
		        .setContentIntent(pIntent)
		        .build();
		    NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			// Hide the notification after its selected
		    n.flags |= Notification.FLAG_AUTO_CANCEL;
			notificationManager.notify(0, n);
			}
			}
		    }
		 }
}

package com.example.energy.Notifications;

import java.util.List;

import com.example.energy.FeedbackActivity;
import com.example.energy.HomeActivity;
import com.example.energy.R;
import com.example.model.achievement;
import com.example.model.databaseHelpers.AchievementsHelper;
import com.example.model.enums.Job;
import com.example.model.interfaces.IAchievement;
import com.google.analytics.tracking.android.EasyTracker;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class NotificationRecieverActivity extends Activity {

	String alertType;
	String alertDescription;
	Integer Kettles;
	Integer Lights;
	Integer Television;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		
		Intent intent = getIntent(); 
		int alarm = Integer.parseInt(intent.getStringExtra("Alarm"));
		if(alarm == 0){
			
			NotificationActivity na = new NotificationActivity();
			na.createNotification();
		}
		
		else{
		
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_notification_reciever);	
			
			alertType = intent.getStringExtra("Type");
			alertDescription = intent.getStringExtra("Description");
		    TextView type = (TextView) findViewById(R.id.typeOfAlert);
			TextView description = (TextView) findViewById(R.id.descriptionOfAlert);
			
			type.setText(alertType);
			description.setText(alertDescription);
		
		}
		
		
	}
	
	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }

	//BUTTONS
	public void DoneAlert(View view){
		final Intent intent = new Intent(this, HomeActivity.class);
		
		final AchievementsHelper achieve = new AchievementsHelper(this);
		
		List<IAchievement> a =  achieve.getAllAchievements();
		
		if (a.isEmpty()){
			IAchievement newAchieve = new achievement(0,0,0);
			achieve.addAchievement(newAchieve);
		}
		

		a =  achieve.getAllAchievements();
		

			for (IAchievement aa : a){
				 
				Kettles = aa.getKettles();
				Lights = aa.getLightbulbs();
				Television = aa.getTelevision();
			}
			//lightbulb = 60Watts/hour
			//tv = 150Watts/hour
			//kettle = 50Watts/kettle
			
			if(alertType.equals(Job.Appliance.toString())){
				Kettles = Kettles + 3;
				Lights = Lights + 2;
				Television = Television + 1;
			}
			
			if(alertType.equals(Job.Chargers.toString())){
				Kettles = Kettles + 1;
				Lights = Lights + 2;
			}
			
			if(alertType.equals(Job.Computer.toString())){
				Kettles = Kettles + 5;
				Lights = Lights + 4;
				Television = Television + 2;
			}
			
			if(alertType.equals(Job.Heating.toString())){
				Kettles = Kettles + 10;
				Lights = Lights + 10;
				Television = Television + 5;
			}
			
			if(alertType.equals(Job.Lights.toString())){
				Kettles = Kettles + 1;
				Lights = Lights + 1;
			}
			
			if(alertType.equals(Job.Projectors.toString())){
				Kettles = Kettles + 2;
				Lights = Lights + 2;
				Television = Television + 1;
			}
			
			
			achieve.deleteAll();
			IAchievement newAchieve = new achievement(Kettles,Lights,Television);
			achieve.addAchievement(newAchieve);
		
			//GOOGLE ANALYTICS
			
		
		//once done we want a thank-you message then straight back home
		new AlertDialog.Builder(this)
	    .setTitle("Thank-You").setNeutralButton("ok", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	            startActivity(intent);
	        }
	     })
	    .setIcon(R.drawable.ic_action_star)
	     .show();
		
		
		
	}
	
	public void IgnoreAlert(View view){
		final Intent intent = new Intent(this, HomeActivity.class);
		
		
		
		
		//once done we want a thank-you message then straight back home
		new AlertDialog.Builder(this)
	    .setTitle("Sorry for disturbing you").setNeutralButton("ok", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	            startActivity(intent);
	        }
	     })
	    .setIcon(R.drawable.ic_action_star)
	     .show();
		
	}
	
	public void SillyAlert(View view){
		
		final Intent intent = new Intent(this, HomeActivity.class);
		final Intent feedback = new Intent(this, FeedbackActivity.class);
		//this should maybe set up email message box where you can say why this was a bad alert so we can keep track
		new AlertDialog.Builder(this)
	    .setTitle("Feedback")
	    .setMessage("Would you like to help us by telling us why this is a silly request?")
	    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	        	feedback.putExtra("Type", alertType);
		    	feedback.putExtra("Description", alertDescription);
	            startActivity(feedback);
	        }
	     })
	    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	        	startActivity(intent);
	        }
	     })
	    .setIcon(R.drawable.ic_action_cut)
	     .show();
	}
}

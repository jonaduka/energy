package com.example.energy;

import com.google.analytics.tracking.android.EasyTracker;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class FeedbackActivity extends Activity {
	String alertType;
	String alertDescription;
	Button buttonSend;
	EditText textMessage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);
		Intent intent = getIntent();        
	    alertType = intent.getStringExtra("Type");
	    alertDescription = intent.getStringExtra("Description");
	    
	    TextView description = (TextView) findViewById(R.id.alertTypeAndDescription);
		description.setText(alertDescription);
		
	    buttonSend = (Button) findViewById(R.id.sendEmail);
		
		
		buttonSend.setOnClickListener(new OnClickListener() {
 
			@Override
			public void onClick(View v) {
			  textMessage = (EditText) findViewById(R.id.editEmailMessage);
				
			  String to = "s0930891@sms.ed.ac.uk";
			  String subject ="SILLY ENERGY NOTIFICATION: " + alertType + ": " + alertDescription;
			  
			  String message = textMessage.getText().toString();
 
			  /*
			  Intent email = new Intent(Intent.ACTION_SEND);
			  email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
			  email.putExtra(Intent.EXTRA_SUBJECT, subject);
			  email.putExtra(Intent.EXTRA_TEXT, message);
 
			  //need this to prompts email client only
			  email.setType("message/rfc822");
 
			  startActivity(Intent.createChooser(email, "Choose an Email client :"));*/
			  Intent email = new Intent(Intent.ACTION_SEND);
			    email.putExtra(Intent.EXTRA_EMAIL, new String[]{to});          
			    email.putExtra(Intent.EXTRA_SUBJECT, subject);
			    email.putExtra(Intent.EXTRA_TEXT, message);
			    email.setType("message/rfc822");
			    //startActivity(Intent.createChooser(email, "Choose an Email client :"));
			    try {
		    		startActivityForResult(Intent.createChooser(email,  "Send email"), 1);
		    		
		    	
		    	} catch (android.content.ActivityNotFoundException ex) {
		    	    Toast.makeText(FeedbackActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
		    	}

		    	}
		

		});
	}
	
	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	  


}

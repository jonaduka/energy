package com.example.energy;

import java.util.Random;

import com.example.energy.Notifications.NotificationActivity;
import com.example.energy.Notifications.TimeAlarm;
import com.google.analytics.tracking.android.EasyTracker;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateUtils;
import android.view.View;

public class HomeActivity extends Activity {
	
	AlarmManager am;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

    	am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		setRepeatingAlarm();
    
	}
	
	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	  
	
    public void registerOffice(View view){
    	Intent intent = new Intent(this, OfficeLocationActivity.class);
    	startActivity(intent); 	
    }
    
    public void goSettings(View view){
    	Intent intent = new Intent(this, SettingsHomeActivity.class);
    	startActivity(intent); 	
    }
    
    /*
    public void notifications(View view){

		Intent intent = new Intent(this, NotificationActivity.class);
    	startActivity(intent);
    	
    }*/
	
    
    
	public void setRepeatingAlarm() {
		//only do this one out of 3 times?
		
		
		
		  Intent intent = new Intent(this, TimeAlarm.class);
		  //intent.putExtra("Alarm", "0");
		  PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
		    intent, PendingIntent.FLAG_CANCEL_CURRENT);
		  am.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
				  AlarmManager.INTERVAL_HOUR, pendingIntent);
		  
		}
    	
    	
    	
    	
    public void achievements(View view){
    	Intent intent = new Intent(this, AchievementsActivity.class);
    	startActivity(intent); 	
    }
    public void help(View view){
    	Intent intent = new Intent(this, HelpActivity.class);
    	startActivity(intent); 	
    }
    
}

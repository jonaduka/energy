package com.example.energy.Solver;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import android.content.Context;
import android.os.Bundle;
import java.util.List;

import com.data.parseRoomsCSV;
import com.example.model.interfaces.IRooms;

import com.data.parseRoomsCSV;
import com.example.model.alert;
import com.example.model.roomAllocations;
import com.example.model.rooms;
import com.example.model.workingHours;
import com.example.model.databaseHelpers.ConfigurationsHelper;
import com.example.model.databaseHelpers.OfficeLocationHelper;
import com.example.model.databaseHelpers.WorkingHoursHelper;
import com.example.model.enums.Day;
import com.example.model.enums.Job;
import com.example.model.enums.RoomType;
import com.example.model.interfaces.IAlert;
import com.example.model.interfaces.IConfigurations;
import com.example.model.interfaces.IRoomAllocations;
import com.example.model.interfaces.IRooms;
import com.example.model.interfaces.IWorkingHours;

public class CalculateAlerts {
	
	IAlert newAlert = new alert();
	//get current date & time
	Calendar c = Calendar.getInstance();
	IWorkingHours wh = new workingHours();
	Day day   = wh.toDayFromInt(c.get(Calendar.DAY_OF_WEEK));
    int hour   = c.get(Calendar.HOUR_OF_DAY);
    int minute = c.get(Calendar.MINUTE);
    Context context;
    
    //if late in the day (if they have working hours) we ask them to turn off heating/lights/computers
    //get working hours for the current day
    // 0 - not working so we shall not disturb them
    // 1 - at the beginning of the day
    // 2 - lunchtime
    // 3 - end of the day
    // 4 - random time of day
    public int getTimeOfDay() {

    	WorkingHoursHelper hours = new WorkingHoursHelper(context);
    	List<IWorkingHours> hoursList = hours.getAllWorkingHours();
    
    	
    	for(IWorkingHours w : hoursList){
    		if (w.getDay().equals(day)){
    			wh = w;
    		}
    	}
    	int now = hour*100 + minute;
    	
    	if(hoursList.isEmpty()){
    		//this is when we assume 9-5 working day
	    	//check if morning, i.e. 30 min around start time
	    	if((now-900)<30){
	    		return 1;
	    	}
	    	//check if lunchtime (between 11:45 and 1:15)
	    	if(1145<now && now<1315){
	    		return 2;
	    	}
	    	//check if end of day i.e. around 30 min before leaving
	    	if((1700-now)<30){
	    		return 3;
	    	}
	    	else{
	    		return 4;
	    	}
    	}
    	else {
	    	int start = wh.getStart();
	    	int end = wh.getEnd();
	    	
	    	
	    	//check if working that day also check that we don't disturb them outside office hours
	    	
	    	if (wh.getIsOn()==false){
	    		if(now<start || now>end){
	    			return 0;
	    		}
	    	}
	    	//check if morning, i.e. 30 min around start time
	    	if((now-start)<30){
	    		return 1;
	    	}
	    	//check if lunchtime (between 11:45 and 1:15)
	    	if(1145<now && now<1315){
	    		return 2;
	    	}
	    	//check if end of day i.e. around 30 min before leaving
	    	if((end-now)<30){
	    		return 3;
	    	}
	    	else{
	    		return 4;
	    	}
    	}
    	
    }
    //depending on time of day (as calculated above) we want to get a random type of job
    //these selections seem to make the most sense to me
    public Job getTypeOfJob(int timeOfDay){
    	List<Job> jobs = new ArrayList<Job>();
    	//morning
    	if(timeOfDay == 1){
    		jobs.add(Job.Heating);
    		jobs.add(Job.Appliance);
    	}
    	//lunch
    	if(timeOfDay == 2){
    		jobs.add(Job.Appliance); 
    		jobs.add(Job.Chargers);
    	}
    	//end of day
    	if(timeOfDay == 3){
    		jobs.add(Job.Heating);
    		jobs.add(Job.Lights);
    		jobs.add(Job.Projectors);
    		jobs.add(Job.Computer);
    		jobs.add(Job.Chargers);
    	}
    	//if its a random time of day
    	else {
    		jobs.add(Job.Heating);
    		jobs.add(Job.Lights);
    		jobs.add(Job.Projectors);
    		jobs.add(Job.Appliance);
    	}
    
        
        
        //get rid of jobs that they dont want to do from this returned list
        ConfigurationsHelper ch = new ConfigurationsHelper(context);
        List<Job> nJobs = new ArrayList<Job>();
        for(IConfigurations a : ch.getAllConfigurations()){
        	if (!a.getComputersOn()){
        		nJobs.add(Job.Computer);
        	}
        	if (!a.getChargersOn()){
        		nJobs.add(Job.Chargers);
        	}
        	if (!a.getHeatingOn()){
        		nJobs.add(Job.Heating);
        	}
        	if (!a.getLightsOn()){
        		nJobs.add(Job.Lights);
        	}
        	if (!a.getProjectorsOn()){
        		nJobs.add(Job.Projectors);
        	}
        }
        	
        for(Job a : nJobs){
        	if (jobs.contains(a)){
        		jobs.remove(a);
        	}
        }
        
    	//chose random from list
        Random randomGenerator = new Random();
        int index = randomGenerator.nextInt(jobs.size());
        
        return jobs.get(index);
     }
    
   public int getQuadrant(){
	   //each floor will be divided into quadrants so each person can only be alerted about a specific quadrant
	   
   	OfficeLocationHelper office = new OfficeLocationHelper(context);
   	List<IRoomAllocations> rooms = office.getAllOfficeLocations();
   	IRoomAllocations room = new roomAllocations();
   	for(IRoomAllocations r : rooms){
   		room = r;
   	}
   	//now that we have their room we return their quadrant. 
   	
    Integer number = room.getRoom() % 100;
    
    if(number<12){
    	return 1;
    }
    if(13<number && number<23){
    	return 2;
    }
    if(24<number && number<31){
    	return 3;
    }
   	if(32<number){
   		return 4;
   	}
   	else return 0;
   	
   }
   
   
   public rooms getRandomRoomFromQuadrantAndType(int quadrant, Job type){
	   
	   //to pick a random room within our quadrant that has the 'job' we want we make a list 
	   //of all the possabilities and pick a random
	   parseRoomsCSV csv = new parseRoomsCSV();
	   String csvFile = getCSVfromInput();
	   List<IRooms> allRooms = csv.run(csvFile, context);
	   List<IRooms> selectRooms = new  ArrayList<IRooms>();
	   
	   OfficeLocationHelper olh = new OfficeLocationHelper(context);
	   IRoomAllocations office = olh.getAllOfficeLocations().get(0);
	   
	   
	   if(!allRooms.equals(null)){
		   
	   
	   for (IRooms r : allRooms){
		   if(r.getQuadrant() == quadrant){
			   if(getTypes(r.getRoomType()).contains(type)){
				   if(r.getRoomType()==RoomType.Office && r.getFloor()==office.getRoom()/100 && r.getRoom()==office.getRoom()%100){
					   //this is your office so fine
					   selectRooms.add(r); 
				   }
				   else if(r.getRoomType()==RoomType.Office){
					   //do nothing since this is not your office
				   }
				   else{
					   selectRooms.add(r);
				   }
			   }
		   }
	   }
	   }
	   Random randomGenerator = new Random();
	   if(selectRooms.size()!=0){
		   int index = randomGenerator.nextInt(selectRooms.size());
		   return (rooms) selectRooms.get(index);
	   }
	   else {
		   int index = randomGenerator.nextInt(allRooms.size());
		   return (rooms) allRooms.get(index);
	   }
       //}
       /*
       
       //BAD CODE TO BE DELETED ONCE CSV IS FIXED
       rooms room = new rooms();
       room.setId(1);
       room.setFloor(3);
       room.setRoom(22);
       room.setRoomType(RoomType.Office);
       
       return room;
       */
   }
   
   //get types of jobs we can have depending on room
   public List<Job> getTypes(RoomType roomType){
	   //Office, Storage, Kitchen, Meeting, Printing, Subject, Pantry, Open;
	   List<Job> jobs = new ArrayList<Job>();
	  if(roomType.equals(RoomType.Kitchen) || roomType.equals(RoomType.Open)){
		  jobs.add(Job.Appliance);
		  jobs.add(Job.Lights);
	  }
	  if(roomType.equals(RoomType.Meeting)){
		  jobs.add(Job.Projectors);
		  jobs.add(Job.Lights);
		  jobs.add(Job.Heating);
		  jobs.add(Job.Computer);
	  }
	  if(roomType.equals(RoomType.Office)){
		  jobs.add(Job.Chargers);
		  jobs.add(Job.Computer);
		  jobs.add(Job.Lights);
	  }
	  if(roomType.equals(RoomType.Pantry)){
		  jobs.add(Job.Lights);
	  }
	  if(roomType.equals(RoomType.Printing) || roomType.equals(RoomType.Subject) || roomType.equals(RoomType.Storage)){
		  jobs.add(Job.Lights);
	  }
	   return jobs;
   }
   
   //THIS IS WHERE WE CREATE THE ACTUAL ALERT
   
   public IAlert createAlert(Context context){
	   this.context = context;
	   IAlert newAlert = new alert();
	   Job job = getTypeOfJob(getTimeOfDay());
	   rooms room = getRandomRoomFromQuadrantAndType(getQuadrant(), job);
	   
	   newAlert.setType(job.name());
	   newAlert.setDescription(toReadableString(room, job));
	   return newAlert;
   }
    
   //how to choose which csv to read depending on which floor your office is on
   public String getCSVfromInput(){
	   
	   final OfficeLocationHelper ol = new OfficeLocationHelper(context);
	  List<IRoomAllocations> r =  ol.getAllOfficeLocations();
	  
	  for (IRoomAllocations a : r){
		  if ((a.getRoom()/100) == 1) {
			  return "floorOne.csv";
		  }
		  if ((a.getRoom()/100) == 2) {
			  return "floorTwo.csv";
		  }
		  if ((a.getRoom()/100) == 3) {
			  return "floorThree.csv";
		  }
		  else {
			  return "floorFour.csv";
		  }
	  }
	  return null;
   }
   
   //THIS IS HOW WE MAKE IT INTO A PRETTY ALERT
   public String toReadableString(rooms room, Job job){
	   String description = new String();
	   if(job.equals(Job.Appliance)){
		   description = "Please check and turn off any appliances in " + room.getFloor() + "." + room.getRoom();
	   }
	   if(job.equals(Job.Chargers)){
		   description = "Please unplug or turn off any chargers in " + room.getFloor() + "." + room.getRoom();
	   }
	   if(job.equals(Job.Computer)){
		   description = "Please turn any computers and laptops off in " + room.getFloor() + "." + room.getRoom();
	   }
	   if(job.equals(Job.Heating)){
		   description = "Please turn off/down any heating appliances in " + room.getFloor() + "." + room.getRoom();
	   }
	   if(job.equals(Job.Lights)){
		   description = "Please turn off any lights in " + room.getFloor() + "." + room.getRoom();
	   }
	   if(job.equals(Job.Projectors)){
		   description = "Please check and turn off any projectors in " + + room.getFloor() + "." + room.getRoom();
	   }
	   
	   return description;
   }
}

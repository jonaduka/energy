package com.example.energy;

import java.util.ArrayList;
import java.util.List;

import com.example.model.configurations;
import com.example.model.workingHours;
import com.example.model.databaseHelpers.ConfigurationsHelper;
import com.example.model.databaseHelpers.WorkingHoursHelper;
import com.example.model.enums.Day;
import com.example.model.interfaces.IConfigurations;
import com.example.model.interfaces.IWorkingHours;
import com.google.analytics.tracking.android.EasyTracker;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class WorkingHoursActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_working_hours);

		//get summary of current working hours if they exist and display them from the database in the right fields.
		
		
		
		 TextView monday = (TextView) findViewById(R.id.mondayWorkingHours);
		 TextView tuesday = (TextView) findViewById(R.id.tuesdayWorkingHours);
		 TextView wednesday = (TextView) findViewById(R.id.wednesdayWorkingHours);
		 TextView thursday = (TextView) findViewById(R.id.thursdayWorkingHours);
		 TextView friday = (TextView) findViewById(R.id.fridayWorkingHours);
		 
		final WorkingHoursHelper hours = new WorkingHoursHelper(this);
					

			if (hours.getAllWorkingHours().size()!=5){
				//clear any values allready in there
				//we shouldnt need this normally but just to make it cleaner for testing atm
				hours.deleteAll();
				
				//initialize the database all with true values
				List<workingHours> newHours = new ArrayList<workingHours>();
				
				
				for(Day d : Day.values()){
					workingHours newH = new workingHours(d,true,900,1700);
					newHours.add(newH);
				}
				
				//add to the database
				for (IWorkingHours w : newHours){
					hours.addWorkingHours(w);
				}
			}
			
		List<IWorkingHours> workingHours = hours.getAllWorkingHours();
		
	    for (Day d : Day.values()){
	    	for(IWorkingHours w : workingHours){
	    		if (d == w.getDay()){
	    			if(d == Day.Monday){
	    				if(w.getIsOn() == false){
	    					monday.setText("Day off");
	    				}
	    				else {
	    					monday.setText("Start work : " + w.getStart() + " and end work at : " + w.getEnd());
	    				}
	    			}
	    			if(d == Day.Tuesday){
	    				if(w.getIsOn() == false){
	    					tuesday.setText("Day off");
	    				}
	    				else {
	    					tuesday.setText("Start work : " + w.getStart() + " and end work at : " + w.getEnd());
	    				}
	    			}
	    			if(d == Day.Wednesday){
	    				if(w.getIsOn() == false){
	    					wednesday.setText("Day off");
	    				}
	    				else {
	    					wednesday.setText("Start work : " + w.getStart() + " and end work at : " + w.getEnd());
	    				}
	    			}
	    			if(d == Day.Thursday){
	    				if(w.getIsOn() == false){
	    					thursday.setText("Day off");
	    				}
	    				else {
	    					thursday.setText("Start work : " + w.getStart() + " and end work at : " + w.getEnd());
	    				}
	    			}
	    			if(d == Day.Friday){
	    				if(w.getIsOn() == false){
	    					friday.setText("Day off");
	    				}
	    				else {
	    					friday.setText("Start work : " + w.getStart() + " and end work at : " + w.getEnd());
	    				}
	    			}
	    		}
	    	}
	    }
	
	}
	
	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	  
	
	
	
	// if we want to edit these values we go to seperate page depending on what button is pressed.
	    
	    public void monday(View view){
	    	Intent intent = new Intent(this, EditWorkingHoursActivity.class);
	    	intent.putExtra("Day", "1");
	    	startActivity(intent); 	
	    }
	    public void tuesday(View view){
	    	Intent intent = new Intent(this, EditWorkingHoursActivity.class);
	    	intent.putExtra("Day", "2");
	    	startActivity(intent); 	
	    }
	    public void wednesday(View view){
	    	Intent intent = new Intent(this, EditWorkingHoursActivity.class);
	    	intent.putExtra("Day", "3");
	    	startActivity(intent); 	
	    }
	    public void thursday(View view){
	    	Intent intent = new Intent(this, EditWorkingHoursActivity.class);
	    	intent.putExtra("Day", "4");
	    	startActivity(intent); 	
	    }
	    public void friday(View view){
	    	Intent intent = new Intent(this, EditWorkingHoursActivity.class);
	    	intent.putExtra("Day", "5");
	    	startActivity(intent); 	
	    }
	    
	    public void doneHours(View view){
	    	Intent intent = new Intent(this, SettingsHomeActivity.class);
	    	startActivity(intent);
	    }

}

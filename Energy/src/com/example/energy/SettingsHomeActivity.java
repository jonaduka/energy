package com.example.energy;

import java.util.GregorianCalendar;

import com.google.analytics.tracking.android.EasyTracker;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class SettingsHomeActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings_home);
	}
	
	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	  
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings_home, menu);
		return true;
	}
	
    public void reminders(View view){
    	Intent intent = new Intent(this, ConfigurationsActivity.class);
    	startActivity(intent); 	
    }

    public void workinghours(View view){
    	Intent intent = new Intent(this, WorkingHoursActivity.class);
    	startActivity(intent); 	
    }

    
    public void doneSettings(View view){
    	Intent intent = new Intent(this, HomeActivity.class);
    	startActivity(intent); 	
    }
    
    public void disturb(View view){
    	Intent intent = new Intent(this, DisturbActivity.class);
    	startActivity(intent);
    }
}

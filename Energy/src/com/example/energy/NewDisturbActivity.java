package com.example.energy;

import com.example.model.doNotDisturb;
import com.example.model.databaseHelpers.DisturbHelper;
import com.example.model.databaseHelpers.WorkingHoursHelper;
import com.example.model.interfaces.IDoNotDisturb;
import com.example.model.interfaces.IWorkingHours;
import com.google.analytics.tracking.android.EasyTracker;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.TimePicker;

public class NewDisturbActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_disturb);
		
		
		TimePicker startTime = (TimePicker) findViewById(R.id.disturbStart);
		TimePicker endTime = (TimePicker) findViewById(R.id.disturbEnd);
		
		endTime.setIs24HourView(true);
		startTime.setIs24HourView(true);
		
		//we are making a new one so just put random values to start
		//9-5 is the default
		startTime.setCurrentHour(9);
		startTime.setCurrentMinute(0);
		endTime.setCurrentHour(17);
		endTime.setCurrentMinute(0);
	}
	
	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	  

	//if they click the save button, here is where we save the new do not disturb and add it to the list
	public void saveDisturb(View view){
		//get and save values
		TimePicker startTime = (TimePicker) findViewById(R.id.disturbStart);
		TimePicker endTime = (TimePicker) findViewById(R.id.disturbEnd);
		
		final DisturbHelper hours = new DisturbHelper(this);
		
		int startHour = startTime.getCurrentHour();
		int startMinute = startTime.getCurrentMinute();
		int endHour = endTime.getCurrentHour();
		int endMinute = endTime.getCurrentMinute();
		IDoNotDisturb dnd = new doNotDisturb();
		dnd.setEnd(endHour*100 + endMinute);
		dnd.setStart(startHour*100 + startMinute);
		
		hours.addWorkingHours(dnd);
		
		//go back to previous screen
		Intent intent = new Intent(this, DisturbActivity.class);
    	startActivity(intent);
	}
}

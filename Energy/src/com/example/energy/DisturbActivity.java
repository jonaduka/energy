package com.example.energy;

import java.util.ArrayList;
import java.util.List;

import com.example.model.databaseHelpers.DisturbHelper;
import com.example.model.databaseHelpers.StableArrayAdapter;
import com.example.model.databaseHelpers.WorkingHoursHelper;
import com.example.model.interfaces.IDoNotDisturb;
import com.google.analytics.tracking.android.EasyTracker;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class DisturbActivity extends Activity {
	
	//TODO make sure we know where the admin offices are. I.E. never disturb here because it might be detrimental

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_disturb);
		
		ListView listview = (ListView) findViewById(R.id.disturbList);
		
		final DisturbHelper hours = new DisturbHelper(this);
		final List<IDoNotDisturb> d = hours.getAllDisturbs();
		
		if(!d.isEmpty()){
			
			ArrayList<String> list = new ArrayList<String>();
			
			for(IDoNotDisturb dnd : d){
				list.add(toReadableString(dnd.getStart(),dnd.getEnd()));
			}
			
			
			final StableArrayAdapter adapter = new StableArrayAdapter(this,android.R.layout.simple_list_item_1, list);
		    listview.setAdapter(adapter);
		}
		else {
			String[] values = new String[] { "No Entries" };

			    final ArrayList<String> list = new ArrayList<String>();
			    for (int i = 0; i < values.length; ++i) {
			      list.add(values[i]);
			    }
			    final StableArrayAdapter adapter = new StableArrayAdapter(this,
			        android.R.layout.simple_list_item_1, list);
			    listview.setAdapter(adapter);
			
		}
		
		listview.setClickable(true);
		listview.setTextFilterEnabled(true);
		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				this);
		 
		listview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
			//get the item that you have clicked
			final IDoNotDisturb selectedFromList =(d.get(position));
			
			//then check that you acctualy want to delete it
			alertDialogBuilder
		    .setTitle("Confirm Deletion")
		    .setMessage("Are you sure you want to delete this time?")
		    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		        	hours.deleteValue(selectedFromList.getStart());
		        	//dialog.dismiss();
		        	Intent intent = getIntent();
		            finish();
		            startActivity(intent);
		        }
		     })
		    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		        	dialog.dismiss();
		        }
		     })
		    .setIcon(R.drawable.ic_action_cut)
		     .show();
			    
			}
		});
		
	}
	
	
	public void newDisturb(View view){
    	Intent intent = new Intent(this, NewDisturbActivity.class);
    	startActivity(intent);
    }
	
	public void finishDisturb(View view){
    	Intent intent = new Intent(this, SettingsHomeActivity.class);
    	startActivity(intent);
    }
	

	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	  
	  
	
	public String toReadableString(Integer start, Integer end){
		
		String s1 = Integer.toString(start % 100);
		if(s1.length() == 1){
			s1 = "0" + s1;
			
		}
		
		String s2 = Integer.toString(end % 100);
		if(s2.length() == 1){
			s2 = "0" + s2;
			
		}
		
		String read = start/100 + ":" + s1  + " untill " + end/100 + ":" + s2  ;
				
		return read;
	}
	
	
	
}

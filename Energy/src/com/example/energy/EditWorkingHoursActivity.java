package com.example.energy;

import com.example.model.workingHours;
import com.example.model.databaseHelpers.WorkingHoursHelper;
import com.example.model.enums.Day;
import com.example.model.interfaces.IWorkingHours;
import com.google.analytics.tracking.android.EasyTracker;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.TimePicker;

public class EditWorkingHoursActivity extends Activity {
	
	int startHour;
	int startMinute;
	int endHour;
	int endMinute;
	Day day;
	Boolean isWorking;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_working_hours);
		
	
		TimePicker startTime = (TimePicker) findViewById(R.id.startTime);
		TimePicker endTime = (TimePicker) findViewById(R.id.endTime);
		CheckBox working = (CheckBox) findViewById(R.id.working);
		
		IWorkingHours wh = new workingHours();
		
		Intent intent = getIntent();        
	    int day1 = Integer.parseInt(intent.getStringExtra("Day"));
	    day = wh.toDayFromInt(day1);
	    
		TextView whichDay = (TextView) findViewById(R.id.whichDay);
		whichDay.setText(day.toString());

		endTime.setIs24HourView(true);
		startTime.setIs24HourView(true);
		final WorkingHoursHelper hours = new WorkingHoursHelper(this);

		for (IWorkingHours w : hours.getAllWorkingHours()){
			//dont forget about 12-24 hour clocks?
			if (w.getDay()==day){
				startHour = w.getStart()/100;
				String startmin = w.getStart().toString().substring(Math.max(w.getStart().toString().length() - 2, 0));
				startMinute = Integer.parseInt(startmin);
				endHour = w.getEnd()/100;
				String endmin = w.getEnd().toString().substring(Math.max(w.getEnd().toString().length() - 2, 0));
				endMinute = Integer.parseInt(endmin);
				isWorking = w.getIsOn();
			}
		}
		startTime.setCurrentHour(startHour);
		startTime.setCurrentMinute(startMinute);
		endTime.setCurrentHour(endHour);
		endTime.setCurrentMinute(endMinute);
		working.setChecked(!isWorking);
	}
	
	 @Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	  
	
    public void save(View view){
    	TimePicker startTime = (TimePicker) findViewById(R.id.startTime);
		TimePicker endTime = (TimePicker) findViewById(R.id.endTime);
		CheckBox working = (CheckBox) findViewById(R.id.working);

    	//save the new working hour values in the database
		final WorkingHoursHelper hours = new WorkingHoursHelper(this);

		for(IWorkingHours w: hours.getAllWorkingHours()){
			if(w.getDay()==day){
				startHour = startTime.getCurrentHour();
				startMinute = startTime.getCurrentMinute();
				endHour = endTime.getCurrentHour();
				endMinute = endTime.getCurrentMinute();
				
				hours.updateValue(day, !working.isChecked(), startHour*100 + startMinute, endHour*100 + endMinute);
			}
			
		}
		
		//hopefully this is all we need to do to set and we dont need to do any funky database manipulations we shall see
    	//NOPE we need to update the database
		
		
    	//go back to previous screen
    	Intent intent = new Intent(this, WorkingHoursActivity.class);
    	startActivity(intent); 	
    }
	

}
